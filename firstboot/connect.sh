#! /bin/sh

# Wrapper script for the git installation and ssh keyfile generation.
. ./install_bash
. ./install_git
. ./ssh_config

install_bash

install_repo_tools

create_keypair
