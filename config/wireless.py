#! /usr/bin/env python3
#
# Read the wireless network configuration file from /etc/buoy/wireless
# and set up the defined networks.

import json
import subprocess

def uci_add(cmd_list):
	uci_add = ['uci', 'add'] + cmd_list
	return sys_cmd(uci_add)


def uci_set(cmd_list):
	uci_set = ['uci', 'set'] + cmd_list
	return sys_cmd(uci_set)

# Run a command on the system and clean up response.
# TODO - add exception handling.
def sys_cmd(cmd_list):
	return subprocess.check_output(cmd_list).decode("utf-8").strip('\n')

def process_wireless(wifi_dict):
	# Dispose of all existing networks.
	# How many are there?
	net_count = sys_cmd(['grep', '-c', 'wifi-config', '/etc/config/wireless'])
	net_id_s = []
	for network in range(int(net_count)):
		resp = sys_cmd(['uci', 'show', 'wireless.@wifi-config[' + str(network) + '].ssid'])
		# Response is in the form "wireless.cfg05fbf7.ssid='xpivot'"
		net_id_list = resp.split('.')
		net_id_s.append(net_id_list[1])

	# Delete the existing networks
	for idx in range(len(net_id_s)):
		sys_cmd(['uci', 'delete', 'wireless.' + net_id_s[idx]])

	# Provision the new networks.	
	for k in wifi_dict.keys():

		# Get the network params.
		params = wifi_dict.get(k)
		
		# The 'id_network' param is used to perform all set operations.
		id_network = uci_add(['wireless', 'wifi-config']) 
		uci_set(['wireless.' + id_network + '.ssid=' + params.get('ssid')])
		uci_set(['wireless.' + id_network + '.encryption=' + params.get('encryption')])
		uci_set(['wireless.' + id_network + '.key=' + params.get('key')])
		sys_cmd(['uci', 'commit'])


#************************************************************
buoy_cfg='/etc/buoy/buoy.cfg'

# Default wifi config file.
wifi_cfg='/etc/buoy/wireless.json'

# Find the WiFi config file.
resp = sys_cmd(['grep', 'wifi_cfg', buoy_cfg])
# If a swing and a miss, we'll use the default.  Otherwise we'll replace it.
if resp:
	wifi_cfg = resp.split('=')[1]

# Read in the WiFi config file and process the networks.
with open(wifi_cfg, 'rt') as file_cfg:
	try:
		wifi_dict = json.load(file_cfg)
		process_wireless(wifi_dict)
	except ValueError as exc:
		print('Wireless config file [ ' + wifi_cfg + ' ] is not valid JSON.')
		print('Wireless network(s) will not be configured.')

