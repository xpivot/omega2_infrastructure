#!/bin/bash
#
# This application logs uptime related info to a log file.
#
#

# This has now been tested on systems in their first AND subsequent
# days of uptime.

# Get the system ID
hostname=$(uci get system.@system[0].hostname)
#printf "System [ %s ]\n" "$hostname"

# Get date & time
tod=$(date +%Y%m%d'-'%H':'%M':'%S)
#printf "ToD [ %s ]\n" "$tod"

# Get uptime
# Note the -p (pretty) and -s (since) are apparently not supported.
# Count the commas.  If 3, there are no days, just hours.  If 4 commas
# then days are included.  Must parse and print accordingly.
up_days="0"
up_hours=""

up=$(uptime)
commas=$(echo $up | sed 's/[^,]//g' | awk '{print length}')

if [ "$commas" -eq 3 ]; then

	up_hours=$(echo $up | awk -F',' '{print $1}' | awk -F' ' '{print $3}')

elif [ "$commas" -eq 4 ]; then

	up_days=$(echo $up | awk -F',' '{print $1}' | awk -F' ' '{print $3}')
	up_hours=$(echo $up | awk -F',' '{print $2}')
	echo $up_hours | grep min > /dev/null
	if [ "$?" -eq 0 ] ; then
		up_hours=$(echo $up_hours | awk -F=' ' '{print $1}')
	fi

else
	printf "ERROR - unexpected comma count [ %s ] for uptime [ %s ]\n" "$commas" "$up"
	exit 1
fi

# Ensure up_hours is properly formatted.
# The first 59 minutes post boot there will be no hours or delimiting colon.  In the first 9
# minutes post boot the minutes will be in just a single digit.
echo $up_hours | grep ':' > /dev/null
if [ "$?" -ne 0 ] ; then   # Will need to pre-pend hours

	# Get rid of the damn spurious "min" appendages
	up_hours=$(echo $up_hours | sed 's/ min//g')

	if [ "$up_hours" -lt 10 ] ; then
		up_hours='0'$up_hours
	fi

	up_hours='0:'$up_hours
fi


# Put it all together
printf "%s - Host [ %s ] Uptime - days [ %s ] - hr:min [ %s ]\n" "$tod" "$hostname" "$up_days" "$up_hours"

