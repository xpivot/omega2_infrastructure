This application directory will contain application(s) and support
files for rendering the Onion Organic LED (OLED) Expansion.

Limitation(s):

- In text mode only 21 characters can be displayed in 8 rows.


General ideas for display (no order):

---Static
	- Murtech project graphic or company logo
	- hostname & software version
	- IP address

---Dynamic
	- tod & uptime
	- SVS-603 data acquisition status
	- GPS satellite lock status
	- back end server connection status

- 
