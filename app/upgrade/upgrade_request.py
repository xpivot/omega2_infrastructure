#! /usr/bin/env python3
#
# Cron-based check for upgrade eligibility.
# All prints are to stdout with the cron line redirecting to
# the log file.

import re
import sys
import json
import subprocess
import urllib
from urllib import request, parse, error
import fileinput
import tempfile
import tarfile
import bz2
import time
import datetime
from datetime import date

# Run a system command, decode and clean up.
# TODO - add exception handling.
def sys_cmd(cmd_list):
        return subprocess.check_output(cmd_list).decode("utf-8").strip('\n')

# Provides a timestamp nicely formatted for a log file.
def now():
	return datetime.datetime.now().strftime("%Y%m%d-%H:%M:%S.%f") + ' - '


# Buoy config file.
CONFIG_BUOY='/etc/buoy/buoy.cfg'


# Build up the complete URL from scheme, host, and document.  The host
# is derived from one of three sources: (1) command line (testing), (2) config
# file (normal ops), and (3) default hard coded herein.

hostname = subprocess.check_output(["uci", "get", "system.@system[0].hostname"]).decode('utf-8').strip('\n')

# List of upgrade hosts - list filled from /etc/buoy.cfg
hosts=[]

# Software version - actual derived from /etc/buoy.cfg
sw_version='0.0.0-0'

# Upgrade scheme (http or https)
scheme='http'

# Default document - actual derived from /etc/buoy.cfg
document='m_buoy/upgrade_req'

# Parse config file for the goodies
with open(CONFIG_BUOY, 'rt') as cfg:
	for line in cfg:
		if line.startswith('UPGRADE_HOST'):
			components=re.split(r'[=\s]\s*', line)
			hosts.append(components[1])
		elif line.startswith('SW_VERSION'):
			components=re.split(r'[=\s]\s*', line)
			sw_version=components[1]
		elif line.startswith('UPGRADE_SCHEME'):
			components=re.split(r'[=\s]\s*', line)
			scheme=components[1]
		elif line.startswith('UPGRADE_REQ_DOC'):
			components=re.split(r'[=\s]\s*', line)
			document=components[1]


# Override the upgrade hosts from config file if provided on command line.
if len(sys.argv) > 1:
	hosts.clear()
	# Expectation here is EVERY command line parameter is a host for testing.
	for index, host in enumerate(sys.argv):
		hosts.append(host)
	# Get rid or program name at index 0; using enumerate param of 'start=1' didn't work.
	hosts.pop(0)


################################
# Create a client request (POST)

# POST parameters
parms_post = {
	'Buoy-Id' : hostname,
	'Buoy-Version' : sw_version
}
querystring = parse.urlencode(parms_post)
qs_ascii = querystring.encode('ascii')

# Address each host available.  This allows for handoff from one
# domain to another (e.g. xpivotcorp.com to murtech.us).
for host in hosts:
	url=scheme + '://' + host + '/' + document
	print(now() + 'Requesting upgrade at URL [ ' + url + ' ]')

	resp = ''

	try:
		post = request.urlopen(url, data=qs_ascii)
		resp = post.read()
	
	except error.HTTPError as exc_ht:
		print(now() + 'HTTPError - Code [ ' + str(exc_ht.code) + ' ] - Reason [ ' + exc_ht.reason + ' ]')

	except error.URLError as exc_url:
		print(now() + 'URLError - Reason [ ' + exc_url.reason.strerror + ' ]')

	# POST is likely now working.  The only validation on the back end
	# however has been seeing a POST in the apache2 access.log file.
	# The actual content of the POST is not yet validated as there's
	# nothing on the back end to do so.  That will be up next!

	# Parse the response to see if there's an upgrade available
	# https://docs.python.org/3/tutorial/inputoutput.html#saving-structured-data-with-json
	if(resp):
		# For test only
		print('Response to the POST follows:')
		print(resp)

		# There is no 'resp' if one of the exceptions above is caught.  Assuming here that
		# if indeed there is a 'resp' that it is in valid JSON form.  Will now convert it
		# to a dictionary and select parts.
		response = json.loads(resp)

		# Is there an upgrade available?
		if(response.get('available') == 'true'):
			print('Upgrade available from host [ ' + host + ' ]')	

			# Sanity check on server; is it the correct client?
			host_id = response.get('hostId')
			if(not host_id == hostname):
				print('Server replied for the wrong client [ ' + host_id + ' ] ... ')
			else:
				print('Client hostname validated...proceeding with temp directory creation.')

				# If there's an upgrade available, download it to a temp directory.  The temp
				# directory will be automagically deleted.
				with tempfile.TemporaryDirectory() as dir_work:
					print('Created temp work directory [ ' + dir_work + ' ]')
					os.chdir(dir_work)

					# urls is a list; we'll only concern ourselves, for now, with item 0.
					# TODO - upgrade to use subsequent url's upon failure of prior.
					urls = response.get('url')
					url = urls[0]
					# Determine file name for write destination
					components = re.split(r'[/]', url)
					file_name = components[-1]

					print('Downloading archive [ ' + url + ' ]')
					urllib.urlretrieve(url, file_name)

					# Un-tar the archive
					tar = tarfile.open(file_name, "r:bz2")		
					tar.extractall()

					# Install the upgrade
					# Execute the normal new installer.
					os.chdir('embedded')
					install = sys_cmd('./new_install')
										

