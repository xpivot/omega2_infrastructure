Application installation and upgrade protocol.

The installer and upgrader software will walk the subdirectories.  In each
of said subdirectories they will look for exactly one file by the name of
"install.cfg".  That file will describe the tasks to be accomplished.  It
will define things such as file names, install locations, start and stop
levels, etc.  Details follow.

BIN_EXEC={file name} - Executable file name.
BIN_DIR={directory name} - Directory to which the executable is to be installed.
INIT_D={file name} - Init script used to start, stop, restart the service at /etc/init.d/.
RC_D_S={symlink name} - Start script (S??${INIT_D}) as a symlink to INIT_D in /etc/rc.d/.
RC_D_K={symlink name} - Stop script (K??${INIT_D}) as a symlink to INIT_D in /etc/rc.d/.  This is optional.

Note the executable need not be a service started from /etc/init.d/{servicename}
For example, it may be a cron task.  In the non-service case the config file would
contain all elements, keys may not have values (e.g. 'INIT_D=').
