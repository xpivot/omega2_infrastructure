#! /usr/bin/python

# Commonly used functions refactored to here.

import subprocess
import time
import os

def get_timestamp():
	localtime = time.localtime(time.time())
	year = '{:04}'.format(localtime.tm_year)
	month = '{:02}'.format(localtime.tm_mon)
	day = '{:02}'.format(localtime.tm_mday)
	hour = '{:02}'.format(localtime.tm_hour)
	min = '{:02}'.format(localtime.tm_min)
	sec = '{:02}'.format(localtime.tm_sec)
	ts = year + '-' + month + '-' + day + '-'
	ts = ts + hour + ':' + min + ':' + sec
	return ts


# Delete a file safely.
def delete_file_safe(f):
	try:
		os.remove(f)
	except OSError:
		pass


# Get the host name of the system.
def get_hostname():
	return subprocess.check_output(["uci", "get", "system.@system[0].hostname"]).strip('\n')

