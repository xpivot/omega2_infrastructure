#! /bin/bash
#
# Install the system as new.
#
#


# Make all of the required directories.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DIR_BASE=$(cd $DIR; cd ..; pwd)
#printf "Execution directory of [ %s ] is [ %s ]\n" "$0" "$DIR"
DIR_BIN=/usr/bin
DIR_LOG=/root/var/log
DIR_CFG=/etc/buoy
DIR_SRC_APP=${DIR_BASE}/app
DIR_SRC_CFG=${DIR_BASE}/config
DIR_SRC_DISK=${DIR_BASE}/disk
DIR_SRC_SYS_SW=${DIR_BASE}/system_sw
DIR_SRC_UPGRADE=${DIR_BASE}/upgrade
DIR_SRC_INSTALL=${DIR_BASE}/install
DIR_SRC_FIRSTBOOT=${DIR_BASE}/firstboot
DIR_SRC_CRON=${DIR_BASE}/cron


#****************************************
# Required directories for buoy application
# Config files
mkdir -p $DIR_CFG

# Executables
mkdir -p $DIR_BIN

# Logging - directory creation command strings
MK_DIR_BUOY='mkdir -p '$DIR_LOG'/buoy/'
MK_DIR_SYS='mkdir -p '$DIR_LOG'/system/'

# Logging - create directories on install
$($MK_DIR_BUOY)
$($MK_DIR_SYS)

# The logging directories must be recreated after boot, so they
# will be added to /etc/rc.local
RC=/etc/rc.local
TMP=$RC.tmp
grep 'mkdir -p '${DIR_LOG}'/system' $RC > /dev/null
if [ "$?" -ne 0 ] ; then
	cp $RC $RC.$(date +%Y%m%d.%H%M)
	cp $RC $TMP

	sed "s|exit 0|$MK_DIR_SYS\n\nexit 0\n|" $TMP > $RC
	rm $TMP
fi
grep 'mkdir -p '${DIR_LOG}'/buoy' $RC > /dev/null
if [ "$?" -ne 0 ] ; then                                       
        cp $RC $RC.$(date +%Y%m%d.%H%M)                        
        cp $RC $TMP                                            
                                                               
        sed "s|exit 0|$MK_DIR_BUOY\n\nexit 0\n|" $TMP > $RC      
        rm $TMP                                                
fi



#*****************************************
# PATH update
echo $PATH | grep $DIR_BIN > /dev/null
if [ $? -ne 0 ] ; then
	PATH=${PATH}:$DIR_BIN
fi


#******************************************
# Ensure disk space is enlarged.  Do not proceed if there is an issue.
cd $DIR_SRC_DISK
. expand_mem
expand_mem
if [ $? -ne 0 ] ; then
	printf "Memory expansion did not succeed.  Cannot install system.  Exiting...\n"
	exit 1
else
	printf "Memory configuration validation is complete.\n"
fi
cd $DIR

#******************************************
# Configure system.

# General config file.
cp $DIR_SRC_CFG/buoy.cfg $DIR_CFG

# Time zone.
. $DIR_SRC_CFG/timezone
set_timezone

#WiFi networks
cp $DIR_SRC_CFG/wireless.json $DIR_CFG
$DIR_SRC_CFG/wireless.py
# Install system software.
$DIR_SRC_SYS_SW/system_sw_install.sh
resp=$?
if [ "$resp" -ne 0 ] ; then
	printf "System software installation failed [ %s ]" "%resp"
fi

# Change the default shell to bash.
. $DIR_SRC_FIRSTBOOT/install_bash
install_bash


#*********************************************
# Install applications and establish services.
. $DIR_SRC_INSTALL/services
service_install
if [ $? -eq 0 ] ; then
	printf "Services were installed.\n"
else
	printf "Services were not properly installed.  Exiting...\n"
	exit 1
fi


#**********************************************
# Set up cron processes.

. $DIR_SRC_CRON/cron_config
cron_config $DIR_SRC_CRON/crontab.cfg
if [ $? -eq 0 ] ; then
	printf "Cron tasks were set up or validated.\n"
else
	printf "Cron was not properly set up.  Exiting...\n"
	exit 1
fi

SW_VERSION=$(awk -F= '/SW_VERSION/ { print $2 }' /etc/buoy/buoy.cfg)
printf "\n\n"
printf "*******************************************\n\n"
printf "Software version %s is now installed.\n\n" "$SW_VERSION"
printf "*******************************************\n\n"

